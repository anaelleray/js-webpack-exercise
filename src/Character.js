/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
export class Character {

    /**
     * @param {string} name 
     */
    constructor(name) {
        this.name = name;
        this.health = 100;
        this.hunger = 0;
        this.position = {
            x: 0,
            y: 0
        };
    }
    /**
     * Méthode qui déplace le personnage dans une direction donnée
     * @param {string} direction la direction dans laquelle va le personnage (doit être up, down, left ou right)
     */
    move(direction) {
        // if (direction === 'up') {
        //     this.position.y--;
        // }
        // if (direction === 'down') {
        //     this.position.y++;
        // }
        // if (direction === 'left') {
        //     this.position.x--;
        // }
        // if (direction === 'right') {
        //     this.position.x++;
        // }
        switch (direction) {
            case 'up':
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;

        }
    }
    /**
     * Méthode qui crée l'élément HTML du personnage
     * @returns {Element} l'élément HTML représentant le personnage
     */
    draw() {
        let div = document.createElement('div');
        div.classList.add('character-style');
        
        let pName = document.createElement('p');
        pName.textContent = 'Name : '+ this.name;
        div.appendChild(pName);
        let pHealth = document.createElement('p');
        pHealth.textContent = 'Health : '+ this.health;
        div.appendChild(pHealth);
        let pHunger = document.createElement('p');
        pHunger.textContent = 'Hunger : '+ this.hunger;
        div.appendChild(pHunger);

        div.style.top = this.position.y  + 'vh';
        div.style.left = this.position.x  + 'vw';

        return div;
    }
}
 