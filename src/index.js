/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */
import { Character } from "./Character";

let instance = new Character('Perso 1');

instance.move('right');
instance.move('right');
instance.move('down');

document.body.appendChild(instance.draw());
